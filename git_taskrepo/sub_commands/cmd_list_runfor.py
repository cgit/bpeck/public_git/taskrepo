
# -*- coding: utf-8 -*-

import xml.dom.minidom
from git_taskrepo.command import Command

class List_RunFor(Command):
    """List choices for filtering on runfor"""
    enabled = True

    def options(self):
        self.parser.usage = "%%prog %s" % self.normalized_name

    def run(self, *args, **kwargs):
        self.set_repo(**kwargs)
        self.set_taskrepo(**kwargs)
        conn = self.taskrepo
        with conn:
            cur = conn.cursor()
            cur.execute("SELECT DISTINCT value FROM runfor")
            rows = cur.fetchall()

            for row in rows:
                    print "%s" % row[0]
