
# -*- coding: utf-8 -*-

import xml.dom.minidom
from git_taskrepo.command import Command

class List_Types(Command):
    """List choices to filter on type"""
    enabled = True

    def options(self):
        self.parser.usage = "%%prog %s" % self.normalized_name

    def run(self, *args, **kwargs):
        self.set_repo(**kwargs)
        self.set_taskrepo(**kwargs)
        conn = self.taskrepo
        with conn:
            cur = conn.cursor()
            cur.execute("SELECT DISTINCT value FROM types")
            rows = cur.fetchall()

            for row in rows:
                    print "%s" % row[0]
