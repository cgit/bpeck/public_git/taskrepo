
# -*- coding: utf-8 -*-

import xml.dom.minidom
from git_taskrepo.command import Command

class List(Command):
    """List tasks available in taskrepo. see --help for more options"""
    enabled = True

    def options(self):
        self.parser.usage = "%%prog %s" % self.normalized_name
        self.parser.add_option(
            "--runfor",
            metavar="PACKAGE",
            action="append",
            help="List tasks that should be run for PACKAGE")
        self.parser.add_option(
            "--type",
            metavar="TYPE",
            action="append",
            help="List tasks only of TYPE")
        self.parser.add_option(
            "--job",
            default=False,
            action="store_true",
            help="Generate job xml")
        self.parser.add_option(
            "--origin",
            metavar="ORIGIN",
            help="Specify an alternative ORIGIN to use.")

    def run(self, *args, **kwargs):
        self.set_repo(**kwargs)
        self.set_taskrepo(**kwargs)
        conn = self.taskrepo
        with conn:
            cur = conn.cursor()

            # Which origin should we use.
            if kwargs.get("origin"):
                origin = kwargs.get("origin")
            else:
                cur.execute("SELECT origin FROM config")
                result = cur.fetchone()
                origin = "%s?%s" % (result[0], self.repo.active_branch.name)

            values = []
            joins = []
            where = []
            extra = ""
            if kwargs.get("type"):
                for x in range(0, len(kwargs.get("type"))):
                    joins.append("LEFT JOIN types AS t_%d ON t_%d.task_id = tasks.id" % (x, x))
                    where.append("t_%d.value=?" % (x,))
                    values.append(kwargs.get("type")[x])

            if kwargs.get("runfor"):
                for x in range(0, len(kwargs.get("runfor"))):
                    joins.append("LEFT JOIN runfor AS rf_%d ON rf_%d.task_id = tasks.id" % (x, x))
                    where.append("rf_%d.value=?" % (x,))
                    values.append(kwargs.get("runfor")[x])

            if where:
                extra = ' '.join(joins)
                extra = "%s WHERE %s" % (extra, ' AND '.join(where))

            cur.execute("SELECT name, description, owner FROM tasks %s ORDER BY name" % extra, values)
            rows = cur.fetchall()
            if kwargs.get("job"):
                xmldoc = xml.dom.minidom.Document()
                job = xmldoc.createElement('job')
                recipeset = xmldoc.createElement('recipeSet')
                job.appendChild(recipeset)
                recipe = xmldoc.createElement('recipe')
                recipeset.appendChild(recipe)

            for row in rows:
                if kwargs.get("job"):
                    task = xmldoc.createElement('task')
                    recipe.appendChild(task)
                    task.setAttribute('name',"/%s/%s" % (self.repo.working_tree_dir.split('/')[-1],
                                                         row[0]))
                    fetch = xmldoc.createElement('fetch')
                    task.appendChild(fetch)
                    fetch.setAttribute('url', "%s#%s" % (origin, row[0]))
                else:
                    print "%s\n\t[%s, %s]" % (row[0], row[1], row[2])
            if kwargs.get("job"):
                print job.toprettyxml()
