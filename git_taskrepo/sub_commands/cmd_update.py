
# -*- coding: utf-8 -*-

import sys, os
from git_taskrepo.command import Command
from git_taskrepo.taskrepo import update_taskrepo, parse_testinfo, TRX

class Update(Command):
    """Update Taskrepo for <TASK>"""
    enabled = True

    def options(self):
        self.parser.usage = "%%prog %s [<path/to/task>]" % self.normalized_name

    def run(self, *args, **kwargs):
        self.set_repo(**kwargs)
        self.set_taskrepo(**kwargs)
        if len(args) >= 1:
            taskpath = os.path.normpath(os.path.join(os.getcwd(), args[0]))
        else:
            taskpath = os.getcwd()
        sys.stderr.write("[TaskRepo] Updating %s ... " % taskpath)
        try:
            update_taskrepo(self.repo, self.taskrepo, taskpath)
        except TRX, e:
            sys.stderr.write("FAIL (%s).\n" % e)
            sys.exit(1)
        else:
            sys.stderr.write("done.\n")
