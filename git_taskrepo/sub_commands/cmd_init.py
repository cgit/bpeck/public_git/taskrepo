
# -*- coding: utf-8 -*-

from git_taskrepo.command import Command
from git_taskrepo.testinfo import ParserError
from git_taskrepo.taskrepo import update_taskrepo, parse_testinfo, TRX_Parse, TRX_TestInfo
import sys, os, commands
import sqlite3

def update_file(filename, line_to_add):
    seen=False
    updated=False
    if os.path.exists(filename):
        with open(filename) as f:
            for line in f:
                line = line.rstrip()
                if line == line_to_add:
                    seen=True
                    break
    if seen == False:
        with open(filename,'a') as f:
            f.write("%s\n" % line_to_add)
        updated=True
    return updated

class Init(Command):
    """Init taskrepo"""
    enabled = True

    def options(self):
        self.parser.usage = "%%prog %s" % self.normalized_name
        self.parser.add_option(
            "--origin",
            default=None,
            help="Specify a read only origin. This is needed if your current origin is ssh://"
        )
        self.parser.add_option(
            "--no-import",
            default=False,
            action="store_true",
            help="Do not automatically import all tasks."
        )

    def run(self, *args, **kwargs):
        # get our repo handler
        self.set_repo(**kwargs)

        # make sure origin is usable without authentication
        if not kwargs.get("origin") and not getattr(self.repo.remotes, 'origin', None):
            self.parser.error("Your git repo doesn't have a origin specified.  use --origin")
            
        remote = kwargs.get("origin") or self.repo.remotes.origin.url
        if remote.startswith("ssh://") or remote.startswith("git+ssh://"):
            self.parser.error("remote origin is %s, you must specify an origin that doesn't need authentication. use --origin" % remote)

        print("Initializing taskrepo:")

        # get our taskrepo handler
        self.set_taskrepo(init=True)

        # Initialize the DB with our tables
        with self.taskrepo:
            cur = self.taskrepo.cursor()    
            # Create tables if needed
            cur.execute("CREATE TABLE IF NOT EXISTS config(origin TEXT NOT NULL)")
            cur.execute("CREATE TABLE IF NOT EXISTS tasks(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, description TEXT, owner TEXT)")
            cur.execute("CREATE TABLE IF NOT EXISTS types(task_id INTEGER, value TEXT NOT NULL)")
            cur.execute("CREATE TABLE IF NOT EXISTS runfor(task_id INTEGER, value TEXT NOT NULL)")
            cur.execute("CREATE TABLE IF NOT EXISTS bugs(task_id INTEGER, value TEXT NOT NULL)")
            cur.execute("DELETE FROM config")
            cur.execute("INSERT INTO config(origin) VALUES (?)", (remote,))

        index = self.repo.index
        is_dirty = False

        # Add taskrepo.db to .gitignore
        gitignore = "%s/.gitignore" % self.repo.working_tree_dir
        if update_file(gitignore, "taskrepo.db"):
            print("    - Added taskrepo.db to .gitignore")
            index.add([gitignore])
            is_dirty = True

        # Add testinfo.desc to .gitignore
        gitignore = "%s/.gitignore" % self.repo.working_tree_dir
        if update_file(gitignore, "testinfo.desc"):
            print("    - Added testinfo.desc to .gitignore")
            index.add([gitignore])
            is_dirty = True

        # if we updated the repo then commit it
        if is_dirty:
            assert index.commit("Initialized to use git taskrepo.").type == 'commit'
            print("    - committed to git")

        # Add git hooks to automatically update taskrepo.db
        post_commit_hook = """\
#!/bin/sh

git diff-tree -r --name-only --no-commit-id HEAD@{1} HEAD | \
while read file; do
    echo "$file" | grep 'Makefile$' -q
    if [ $? -eq 0 ]; then
        dirname=$(dirname $file)
        git taskrepo update $dirname
    fi
done
"""
        if not os.path.exists("%s/hooks/post-commit" % self.repo.git_dir):
            with open("%s/hooks/post-commit" % self.repo.git_dir,'w') as f:
                f.write(post_commit_hook)
            os.chmod("%s/hooks/post-commit" % self.repo.git_dir, 0755)
            print("    - Installed post-commit hook")

        post_checkout_hook = """\
#!/bin/sh

previous_head=$1
new_head=$2
is_branch_checkout=$3

git diff-tree -r --name-only --no-commit-id $1 $2 | \
while read file; do
    echo "$file" | grep 'Makefile$' -q
    if [ $? -eq 0 ]; then
        dirname=$(dirname $file)
        git taskrepo update $dirname
    fi
done
"""
        if not os.path.exists("%s/hooks/post-checkout" % self.repo.git_dir):
            with open("%s/hooks/post-checkout" % self.repo.git_dir,'w') as f:
                f.write(post_checkout_hook)
            os.chmod("%s/hooks/post-checkout" % self.repo.git_dir, 0755)
            print("    - Installed post-checkout hook")

        post_merge_hook = """\
#!/bin/sh

git diff-tree -r --name-only --no-commit-id ORIG_HEAD HEAD | \
while read file; do
    echo "$file" | grep 'Makefile$' -q
    if [ $? -eq 0 ]; then
        dirname=$(dirname $file)
        git taskrepo update $dirname
    fi
done
"""
        if not os.path.exists("%s/hooks/post-merge" % self.repo.git_dir):
            with open("%s/hooks/post-merge" % self.repo.git_dir,'w') as f:
                f.write(post_merge_hook)
            os.chmod("%s/hooks/post-merge" % self.repo.git_dir, 0755)
            print("    - Installed post-merge hook")

        # walk the git repo from working_tree_dir and import all tasks
        # unless option --no-import was passed in.
        if kwargs.get("no_import") is False:
            print("    - Importing tasks into taskrepo")
            for (dirpath, dirnames, filenames) in os.walk(self.repo.working_tree_dir):
                try:
                    update_taskrepo(self.repo, self.taskrepo, dirpath)
                except ParserError, e:
                    print >> sys.stderr, ("      - %s FAIL (%s)." % (dirpath, e))
                except TRX_TestInfo:
                    pass
                else:
                    print("      - %s Imported." % dirpath)
        print("Done!")
