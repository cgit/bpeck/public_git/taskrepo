
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import os
import sys
from optparse import Option, IndentedHelpFormatter, SUPPRESS_HELP
from command import CommandOptionParser, CommandContainer
import git.exc

__version__ = '0.1'

__all__ = (
    "main",
)


class BeakerOptionParser(CommandOptionParser):
    standard_option_list = [
    ]

# register default command plugins
import git_taskrepo.sub_commands
CommandContainer.register_module(git_taskrepo.sub_commands, prefix="cmd_")


def main():
    command_container = CommandContainer()
    formatter = IndentedHelpFormatter(max_help_position=60, width=120)
    parser = BeakerOptionParser(version=__version__,
            conflict_handler='resolve',
            command_container=command_container,
            default_command="help", formatter=formatter)

    # Need to deal with the possibility that requests is not importable...
    try:
        import requests
        maybe_http_error = (requests.HTTPError,)
    except ImportError:
        maybe_http_error = ()

    # This is parser.run(), but with more sensible error handling
    cmd, cmd_opts, cmd_args = parser.parse_args()
    try:
        return cmd.run(*cmd_args, **cmd_opts.__dict__)
    except git.exc.InvalidGitRepositoryError, e:
        sys.stderr.write('Not a valid git repo: %s\n' % e)
        return 1
    except git.exc, e:
        sys.stderr.write('GIT error: %s\n' % e)
        return 1
    except ValueError, e:
        sys.stderr.write('%s\n' % e)

if __name__ == '__main__':
    sys.exit(main())
