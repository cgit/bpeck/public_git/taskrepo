Name:		git-taskrepo
Version:	1
Release:	1%{?dist}
Summary:	Creates a taskrepo inside of an existing git repo.

Group:		Development/Libraries
License:	GPLv2+
URL:		http://beaker-project.org/git-taskrepo
Source0:	http://fedorahosted.org/releases/g/i/%{name}-%{version}.tar.gz

BuildArch:	noarch
BuildRequires:	python-devel
BuildRequires:	python-setuptools
Requires:	python-setuptools
Requires:	GitPython >= 1.0.1

%if 0%{?rhel}%{?fedora} <= 6
BuildRequires:  python-ordereddict
BuildRequires:  python-argparse
Requires:       python-ordereddict
Requires:       python-argparse
%endif

%description
This creates and maintains a taskrepo inside of an existing git repo.  Allows
you to easily create xml jobs which can be run with the test harness restraint.

%prep
%setup -q -n %{name}-%{version}

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%files
%attr(0755, root, root)%{_bindir}/%{name}
%{python_sitelib}/git_taskrepo*

%changelog
* Mon May 11 2015 Bill Peck <bpeck@redhat.com> 1-1
- allow overriding the saved origin when generating the job xml.
  (bpeck@redhat.com)
- Keep track of dirty repo on our own.  If repo has no HEAD is_dirty will throw
  a traceback. (bpeck@redhat.com)
- Prevent repo root from being imported. (bpeck@redhat.com)
- fix _delete_taskrepo, referenced taskname from other scope.
  (bpeck@redhat.com)
- Update Requires (bpeck@redhat.com)
- fix authors (bpeck@redhat.com)
- Include python-setuptools (bpeck@redhat.com)
- Build as a noarch (bpeck@redhat.com)
- small help changes in commands (bpeck@redhat.com)
- fix files section in spec (bpeck@redhat.com)
- Updated list tasks (bpeck@redhat.com)

* Wed May 06 2015 Bill Peck <bpeck@redhat.com> 0-1
- new package built with tito
